debian-jessie: debian-jessie-ansible-pip debian-jessie-ansible-repository debian-jessie-ansible-compiled

debian-jessie-ansible-pip:
	docker build --rm=true -t registry.gitlab.com/supernami/docker-ansible-images/debian/jessie:ansible-pip -f debian/jessie/ansible-pip.df .

debian-jessie-ansible-repository:
	docker build --rm=true -t registry.gitlab.com/supernami/docker-ansible-images/debian/jessie:ansible-repository -f debian/jessie/ansible-repository.df .

debian-jessie-ansible-compiled:
	docker build --rm=true -t registry.gitlab.com/supernami/docker-ansible-images/debian/jessie:ansible-compiled -f debian/jessie/ansible-compiled.df .
